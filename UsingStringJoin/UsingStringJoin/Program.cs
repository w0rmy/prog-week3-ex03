﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingStringJoin
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[5] { "red", "rblue", "orange", "white", "black" };
            var join = string.Join(", ", colours);
            Console.WriteLine(join);
        }
    }
}
